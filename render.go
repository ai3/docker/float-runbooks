package main

import (
	"bytes"
	"encoding/json"
	"flag"
	htmltemplate "html/template"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"text/template"

	bf "github.com/russross/blackfriday/v2"
)

// Oh no not another static site generator. Yeah well, we would have
// used Hugo but we need to treat the Markdown *as* a template itself
// (to parameterize links etc), so...

var (
	config       = flag.String("config", "", "JSON `file` with additional parameters")
	srcDir       = flag.String("source", "content", "source `path`")
	dstDir       = flag.String("output", "public", "output `path`")
	templatePath = flag.String("template", "template.html", "template `file`")
)

var (
	htmlTpl      *htmltemplate.Template
	configValues map[string]interface{}
)

func textTemplate(path string) ([]byte, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	tpl, err := template.New("").Parse(string(data))
	if err != nil {
		return nil, err
	}
	var buf bytes.Buffer
	if err := tpl.Execute(&buf, configValues); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func htmlTemplate(vars map[string]interface{}) ([]byte, error) {
	var buf bytes.Buffer
	if err := htmlTpl.Execute(&buf, vars); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

func renderMarkdown(input []byte) []byte {
	renderer := bf.NewHTMLRenderer(bf.HTMLRendererParameters{
		Flags: bf.Smartypants | bf.SmartypantsFractions,
	})
	return bf.Run(
		input,
		bf.WithRenderer(renderer),
		bf.WithExtensions(bf.CommonExtensions),
	)
}

func render(relPath string) error {
	dir, base := filepath.Split(relPath)
	name := strings.TrimSuffix(base, ".md")
	outDir := filepath.Join(*dstDir, dir)
	outPath := filepath.Join(outDir, name+".html")
	baseURI, _ := filepath.Rel(outPath, *dstDir)

	// First level of templating.
	content, err := textTemplate(filepath.Join(*srcDir, relPath))
	if err != nil {
		return err
	}

	// Markdown rendering.
	rendered := renderMarkdown(content)

	// Second level of templating.
	htmlContent, err := htmlTemplate(map[string]interface{}{
		"Title":   name,
		"BaseURI": baseURI,
		"Config":  configValues,
		"Content": htmltemplate.HTML(string(rendered)),
	})
	if err != nil {
		return err
	}

	// Create the target directory if necessary.
	if _, err := os.Stat(outDir); os.IsNotExist(err) {
		if err := os.MkdirAll(outDir, 0755); err != nil {
			return err
		}
	}
	return ioutil.WriteFile(outPath, htmlContent, 0644)
}

func main() {
	flag.Parse()

	if *config != "" {
		data, err := ioutil.ReadFile(*config)
		if err != nil {
			log.Fatalf("could not open config file: %v", err)
		}
		if err := json.Unmarshal(data, &configValues); err != nil {
			log.Fatalf("could not parse config: %v", err)
		}
	}

	var err error
	data, err := ioutil.ReadFile(*templatePath)
	if err != nil {
		log.Fatalf("could not open template: %v", err)
	}
	htmlTpl, err = htmltemplate.New("").Parse(string(data))
	if err != nil {
		log.Fatalf("error parsing HTML template %s: %v", *templatePath, err)
	}

	// Scan the source directory.
	err = filepath.Walk(*srcDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			log.Printf("error scanning %s: %v", path, err)
			return nil
		}
		if info.IsDir() {
			return nil
		}
		relPath, _ := filepath.Rel(*srcDir, path)
		if err := render(relPath); err != nil {
			log.Fatalf("error rendering %s: %v", path, err)
		}
		return nil
	})
	if err != nil {
		log.Fatalf("error scanning %s: %v", *srcDir, err)
	}
}
