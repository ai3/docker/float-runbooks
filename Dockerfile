FROM docker.io/library/golang:1.20.1

COPY . /src
COPY merge-runbooks /usr/bin/merge-runbooks
WORKDIR /src
RUN go build -o /usr/bin/render-runbooks render.go
