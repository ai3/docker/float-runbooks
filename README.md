float-runbooks
===

Base container image to serve alert runbooks as static content. This
image contains some basic instructions for the generic alerts defined
in *float. The idea is that you'd build your own container image,
extending it with your own custom alerts, or additional instructions,
while at the same time benefiting from updates to the "core"
runbooks.

You probably want to create your own container image anyway just to
provide some required configuration parameters such as your domain
name, so that the links used in the runbooks work.

# Creating a custom runbook container image

Set up a new project with whatever source management system you're
using. In there, create a multi-stage Dockerfile similar to the
following:

```
# Use the float-runbooks image as base for the build stage.
# The repository's contents will be in /src.
FROM registry.git.autistici.org/ai3/docker/float-runbooks:master AS build

# Add our custom runbooks to the upstream ones.
COPY content/alerts/ /myalerts
RUN merge-runbooks /myalerts

# Copy our configuration file in place.
COPY config.json /config.json

# Render the website. Create an archive for offline usage.
RUN render-runbooks --config=/config.json
RUN cp -r /src/static/* /src/public/
RUN tar -C /src/public -c -z -v -f /runbooks.tgz .

# Use the static-content simple static HTTP server image, and copy
# the rendered runbooks from the build stage.
FROM registry.git.autistici.org/ai3/docker/static-content:master
COPY --from=build /src/public/ /var/www/
COPY --from=build /runbooks.tgz /var/www/
```

Put your runbook entries in the *content/alerts/* directory of your
repository. Runbook entries should be files named after the alert
itself (preserving capitalization), with a `.md` extension. Their
contents should be Markdown, with an added [Go HTML
template](https://golang.org/pkg/html/template/) syntax to reference
configuration variables. Attributes defined in config.json will be
available as attributes of the top-level "." object, e.g.:

* config.json:

```json
{
  "public_domain": "example.com"
}
```

* runbook file:

```
... {{.public_domain}} ...
```

You can add content to the existing runbook entries just by creating
a file with the same name (e.g. `JobDown.md`), the contents will then
be appended at the end of the existing ones.

## Configuration

The *config.json* file can include any custom variable that you might
need in your templates. Currently, the base image makes use of two
attributes:

* *public_domain* (mandatory), which is used to build the URLs
  pointing back at float infrastructural services (logs, monitoring,
  etc).
* *alert_repository_url*, if present, should point to the URL of your
  custom alerts repository: in this case, an "edit" link will be added
  to every page.

## Customizing appearance

All pages are rendered using a simple [Go HTML
template](https://golang.org/pkg/html/template/) which you can modify
as you like (the default one is pretty bare). You can then either
overwrite /src/template.html in your Dockerfile, or invoke
*render-runbooks* with the *--template* option pointing at your
template.

The template's top-level object has the following attributes:

* *Title* - name of the page, without the `.md` extension
* *Content* - HTML content of the page
* *Config* - contents of config.json
* *BaseURI* - (relative) path to the site root

# Using it with float

In order to integrate with float's alerting system, we're going to
need two things:

* the runbook server must be hosted somewhere online
* the alert templates must point at the location of the runbook server

For simplicity and convenience we're going to host the runbook server
on the float infrastructure itself. To do so, add this to your
*services.yml*:

```yaml
runbooks:
  scheduling_group: frontend
  containers:
    - name: http
      image: my.container.image/url:master
      port: 9933
      env:
        ADDR: ":9933"
  public_endpoints:
    - name: runbooks
      port: 9933
      scheme: http
      enable_sso_proxy: true
```

Finally the configuration should be modified (possibly somewhere under
group_vars/all/) to set:

```yaml
alert_runbook_fmt: "https://runbooks.example.com/alerts/%s.html"
```

Of course in the example above you should replace example.com and the
container URL with values that apply to your situation.
