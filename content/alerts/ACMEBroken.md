It seems that the ACME automation was unable to load or generate any
valid certificate. This usually indicates an unexpected problem with
the *acme* service itself.

Check the logs for

> program:acmeserver

to figure out what might be wrong.

## Further references

* [How ACME automation works in float](https://git.autistici.org/ai3/float/-/blob/master/roles/float-infra-acme/README.md)
