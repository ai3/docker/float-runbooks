A host is experiencing a high rate of network packet drops. This could
be the sign of a hardware problem, or a DDoS. To investigate further,
check out the [Host
dashboard](https://grafana.{{.public_domain}}/d/W8eE_Qgik/host-overview)
for the problematic host to quantify the problem, and look at *dmesg*
output for clues.
