A publicly visible SSL certificate for one of our services will expire
in a few days (according to the blackbox prober).

Public SSL certificates are managed via our [ACME
automation](https://git.{{.public_domain}}/ai3/float/-/blob/master/roles/acme/README.md).

The first issue is to understand which certificate the alert is
talking about: in the case of HTTPS, where we serve many different
certs on the same host:port, you might have to figure it out based on
the probe name. Otherwise the target port should point unambiguously
at the right service.

It is possible that the certificate was indeed renewed but the service
failed to reload it: to verify if this is the case, check for the
absence of [CertRenewalFailures](tech/playbooks/CertRenewalFailures)
alerts for the certificate. Or go looking at `/etc/credentials/public`
on the host. Restarting the service will usually fix the immediate
issue, but one should understand why the automated reload failed in
the first place.

It is also possible that the ACME automation is just not configured to
handle the certificate. To verify, go on the host that runs the *acme*
service, and see if any of the files below */etc/acme/certs.d*
contains the certificate name.

