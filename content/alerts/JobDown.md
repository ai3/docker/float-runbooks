*JobDown* is a very generic alert telling you that Prometheus is
unable to fetch metrics from a target. This generally means that the
process that is supposed to be the target is not running or is
unresponsive, using metrics scraping as a kind of health probe.

As process health isn't necessarily tied to service health or any
user-visible issue, this is an informational alert. The alert also has
multiple levels, spanning from "a specific job is down on a single
host" to "a majority of the instances of this job are down", so read
the description of the alert to figure out which case you're in.

In the hierarchy of alerts, JobDown represents the lowest level: look
for ServiceDegraded or ServiceAvailabilityTooLow alerts in the [alerts
dashboard](https://alerts.{{.public_domain}}) to understand the actual
impact in terms of services.

To check if the target process is running or not, you should first
find the systemd unit or container associated with this target. There
are a few possibilities:

* The service name is often directly encoded in the target host:port,
  e.g. *hostname*.*service*.*domain*. In order to map the port to a
  specific systemd unit, you might need to look up the configuration
  of the service in Ansible.
* Otherwise, the target port should point at the right service by
  checking the [admin
  dashboard](https://admin.{{.public_domain}}) or your
  services.yml float configuration file.
* Some of float's own cluster-level services are not configured in
  services.yml, and they run on all hosts. These currently are the
  *prometheus-node-exporter* (port 9100), the *cgroups-exporter* (port
  3909) and *mtail* (3903).
* Finally, JobDown alerts for jobs that start with `prober` mean that
  the *prometheus-blackbox* jobs themselves are down, rather than the
  target of the probes.

