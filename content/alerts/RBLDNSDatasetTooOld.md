The RBL feeds (these days just URIBL) that we pull from Riseup are
old, which means that the cron job to update them is failing.

It can happen, when a server is restarted unexpectedly, that the
update cron job leaves its lock file around: you can log on the
alerting server and check for the presence of the
`/var/lib/rbldns/uribl/.datafeed.pid` file, and remove it.

In any case, the update cron job does not produce logs, so for
debugging it the simplest thing to do is just to run it manually:

```
su rbldns -c /var/lib/rbldns/sync_uribl_datafeeds.sh
```
