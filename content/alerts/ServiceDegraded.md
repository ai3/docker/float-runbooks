An instance of a service is down (on a specific host).

An "instance" of a float service can be made of multiple systemd
units, and each instance is considered "down" if at least one of its
systemd units is failing.

This alert is just a warning, as in most cases services should be able
to operate correctly even with reduced availability. When a
sufficiently large number of instances are failing for a service, the
[ServiceAvailabilityTooLow](ServiceAvailabilityTooLow.html)
alert will fire.

## Further debugging

To investigate further, look at the [alerts
dashboard](https://alerts.{{.public_domain}}/) and check for JobDown
or SystemdUnitCrashLooping alerts firing for the same host.
