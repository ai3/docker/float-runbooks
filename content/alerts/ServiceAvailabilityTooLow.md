Enough instances of a service are down that its availability is
compromised. This is a global alert and can happen in a few cases:

* the only instance of a single-instance service is down
* more than a certain percentage of instances of a multi-instance
  service are down (40%)

Even though these two cases are expression of the same rule, they do
not have the same urgency: presumably, a singly-homed service is not
expected to have high availability and its failure is less serious
than a replicated service having become unable to self-heal.

While this alert is sourced from job health data, the assumption is
that the scale of the problem is large enough that its users are
experiencing issues.

## Further debugging

An "instance" of a float service can be made of multiple systemd units
or containers, and each instance is considered "unhealthy" if at least
one of its components is failing (see the
[ServiceDegraded](ServiceDegraded.html) alert).

So the first thing to do would be to locate exactly which components,
and on which hosts, are failing specifically. To do so, start with
looking at the list of systemd units and container that constitute the
service, on the [admin
dashboard](https://admin.{{.public_domain}}).

Then look at the [alerts
dashboard](https://alerts.{{.public_domain}}/) and:

* find the problematic hosts by looking for
  [ServiceDegraded](ServiceDegraded.html) alerts
* find the problematic jobs by looking for [JobDown](JobDown.html) or
  [SystemdUnitCrashLooping](SystemdUnitCrashLooping.html) alerts

Once these are found it is possible to go to the [logs
dashboard](https://logs.{{.public_domain}}/app/discover) and look for
the logs of the failing process, with queries such as:

> program:"foo"

or

> program:"foo" AND host:"bar"

## Possible causes

This alert often happens as a consequence of a push, for instance when
a broken configuration or a bad new container image has been pushed to
all the nodes via Ansible. So another good thing to check would be the
history of configuration pushes in the [System Overview
dashboard](https://grafana.{{.public_domain}}/d/b06YjMAZz/system-overview),
or the history of container image versions that can be found in the
[Service Overview
dashboard](https://grafana.{{.public_domain}}/d/xfV2rd7ik/service-overview)
by selecting the appropriate systemd unit.
