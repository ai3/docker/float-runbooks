This alert fires when the [end-to-end syslog
prober](https://git.autistici.org/ai3/tools/dye-injector) on one or
more hosts is failing, meaning that the logs written on those hosts
aren't making it to the Elasticsearch index.

The problem is either with the individual *rsyslog* daemons on the
problematic hosts, or with the central *log-collector-rsyslog* server,
in which case it probably affects all hosts. To figure out more
details, look at the [Syslog
dashboard](https://grafana.{{.public_domain}}/d/vpP0KpSMz/syslog) and
try to spot the problematic hosts by looking for those with large
*omfwd* queues: they're the hosts where *rsyslog* is having trouble
talking to *log-collector-rsyslog*.

Another possibility is that the *log-collector-rsyslog* process itself
is broken: cases have been observed where the process would get
confused about the state of its event loop, and keep using 100% of the
CPU without making any progress. If this is the case, a restart should
fix it:

```
systemctl restart docker-log-collector-rsyslog
```

## Further debugging

* [Syslog dashboard](https://grafana.{{.public_domain}}/d/vpP0KpSMz/syslog)
