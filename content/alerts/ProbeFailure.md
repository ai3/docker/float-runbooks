A blackbox probe, which tests publicly visible endpoints, is
failing. This is usually a real, user-visible issue.

The probe name (*probe* label) and target should give a hint as to
what is wrong. In order to find out more you should visit the [prober
dashboard](https://prober.{{.public_domain}}/), and find the failed
probes there: the details page will contain the full logs of the
failed probe, which should provide enough information. Note that the
prober dashboard only retains logs for a short amount of time, so it's
not easy to figure out the problem unless it's actively ongoing.

This alert will **not** fire if the host is simply unreachable, as it
is inhibited by the HostUnreachable alert. This is to avoid useless
alerting noise that won't add any information.
