A website hosted by us is serving a lot of HTTP 5xx errors (> 20% of
the traffic for the website). It is highly likely that users are
experiencing issues with the site's functionality.

## Further debugging

The alert could be due to a number of reasons. If this is a sudden,
widespread condition across many websites, it is very likely that
something went wrong with the infrastructure itself rather than a
specific website. Look for [ServiceDegraded](ServiceDegraded.html) or
other alerts that may be affecting HTTP services.

The [HTTP
dashboard](https://grafana.{{.public_domain}}/d/xCSUMFnmz/http) for
the affected website should also provide quantitative information (how
many errors, which error codes), and it should allow to pin-point the
start of the problem. But for HTTP traffic, it's best to head directly
to the log analysis service.

### Checking HTTP logs

Head to [the logs dashboard](https://logs.{{.public_domain}}/) to try
to find out where the problem is. Go to *Dashboards > web:overview*
and query for (say for the example.com site):

> vhost:"\*example.com"

(the \* will find requests for *www.example.com* too). We don't yet
have a breakdown by HTTP status, but try to figure out where the >=
500 errors are. The specific status codes can already tell you
something about the location of the error:

* 500: the website is broken (configuration error in Apache, or a PHP
  fatal error)
* 504: there's been a timeout running the PHP script for this request
* 502, 503: the NGINX reverse proxy couldn't talk to the backend
  Apache server, there could be something wrong with the backend
  service itself on the server that hosts this website

For the first two cases, one has to investigate further by looking at
the Apache/PHP logs for the instance that is serving the website. So
first it is necessary to identify which service is responsible for the
broken website, which you can do in the [admin
dashboard](https://admin.{{.public_domain}}).

Then, the [logs
dashboard](https://logs.{{.public_domain}}/app/discover) should allow
you to see what is wrong with the service.

### NGINX error logs

Occasionally the NGINX error log will also have useful information,
this can be accessed by querying e.g.:

> program:"nginx"

on the logs dashboard - unfortunately this logs can't be broken down
or filtered by website.
