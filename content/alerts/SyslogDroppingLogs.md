The log-collector rsyslog daemon is failing at sending logs to
Elasticsearch, which results in logs being lost.

This could be due to a problem with the Elasticsearch server
(*log-collector-elasticsearch*) or with the rsyslog daemon itself
(*log-collector-rsyslog*), which sometimes gets stuck.

Check the [alerts dashboard](https://alerts.{{.public_domain}}) for
anything related to the Elasticsearch server, failing that you can try
kicking the rsyslog daemon (on the appropriate host):

```shell
systemctl restart docker-log-collector-rsyslog
```

## Further debugging

* [Syslog dashboard](https://grafana.{{.public_domain}}/d/vpP0KpSMz/syslog)
