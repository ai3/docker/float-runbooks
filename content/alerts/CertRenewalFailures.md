The ACME automation service has failed to renew a SSL
certificate. To understand why, check the
[logs](https://logs.{{.public_domain}}/) for (assuming the alert is
for example.com)

> program:acmeserver AND "example.com"

and try to find the associated errors.

We use the *http-01* challenge type for ACME, so failure to renew a
certificate is usually due to one of the following things:

### Domain has expired

Check with

```shell
whois example.com
```

if the nameserver records point at the domain registration company, or
just visiting the site and seeing if it shows a domain parking page.

If the domain is expired (and our account automation failed to mark it
as such), you can add it to the list of ACME exceptions by modifying
the `acme_disabled_domains` configuration variable in our float
production config.

### DNS points somewhere else

Check with

```shell
dig +short example.com
```

if the records do not point at our frontend IPs (compare with the
output of `dig +short {{.public_domain}}`), then there is no chance
that we can complete the ACME validation process (i.e. we do not "own"
the domain). Once again, our account automation should automatically
detect this, but if it fails to do so, add the domain to the
`acme_disabled_domains` variable as mentioned above.

## Further references

* [How ACME automation works in float](https://git.autistici.org/ai3/float/-/blob/master/roles/float-infra-acme/README.md)
