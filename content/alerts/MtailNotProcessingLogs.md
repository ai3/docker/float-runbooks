When this alert fires, the *mtail* process on a host has not received
any logs from the local *rsyslog* for some time, which usually
indicates that the communication between rsyslog and mtail (via
/run/mtail.fifo) is somehow stuck.

You can verify this by checking the [Syslog Grafana
dashboard](https://grafana.{{.public_domain}}/d/vpP0KpSMz/syslog) and
looking for Discards for the *action-1-builtin:ompipe* queue.

As a remediation try restarting the *mtail.socket* and
*rsyslog.service* systemd units on the problematic hosts:

```shell
systemctl restart mtail.socket
systemctl restart rsyslog.service
```

## Further debugging

* [Syslog dashboard](https://grafana.{{.public_domain}}/d/vpP0KpSMz/syslog)
