A systemd service is failing to start successfully. Float runs its
systemd units with `Restart=always`, so they will just keep retrying
to start forever.

## Further debugging

There is most likely a configuration issue with the service, check out
the [logs](https://logs.{{.public_domain}}/app/discover) for the
service on the host where it is failing. Use a *program:* filter,
where the program is just the systemd unit name without the *.service*
suffix - for containers, also drop the *docker-* prefix.

## Possible causes

To identify exactly which change caused the problem (and thus which
commit to revert), check the history of configuration pushes in the
[System Overview
dashboard](https://grafana.{{.public_domain}}/d/b06YjMAZz/system-overview),
or the history of container image versions that can be found in the
[Service Overview
dashboard](https://grafana.{{.public_domain}}/d/xfV2rd7ik/service-overview)
by selecting the appropriate systemd unit.
