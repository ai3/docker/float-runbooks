
The monitoring system thinks that a filesystem will soon (in a few
hours) become 100% full, if data keeps being written at the current
rate.

It tries to predict this by looking at recent increases in filesystem
usage, so it is possible that it might get it wrong, for instance:

* the filesystem is tiny (large fluctuations in usage)
* something wrote a large file to disk in a short time

Check the current usage and see if the associated LV needs to be resized.

### Resizing a LV

Resizing a logical volume can be done online (except for the root), as
long as we're increasing its size:

* find the LV that the filesystem is mounted on (it will be something
  like `/dev/vg0/blah`)
* `lvresize -L +50G /dev/vg0/blah` (to increase the size by 50G)
* `screen resize2fs /dev/vg0/blah` (to resize the filesystem, run
  under screen in case of trouble with the ssh connection)
