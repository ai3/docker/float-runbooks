One or more of our scheduled backups have failed. To find out details,
go to https://logs.{{ .public_domain }}/ -> Discover -> search for
"program:tabacco" (maybe also add "AND host:something" to limit the
query to the host that is complaining).

Make sure to extend the query time frame from the default of "last 15
minutes" to cover the time when the backup cron job might have run, so
make sure you're looking at least one-two days in the past, or the
query will show no results.
